package main

import (
	"strings"
	"syscall/js"

	"github.com/google/licensecheck"
)

func getElementByID(id string) js.Value {
	return js.Global().Get("document").Call("getElementById", id)
}

func checkLicense(text string) (ids []string) {
	coverage := licensecheck.Scan([]byte(text))

	ids = make([]string, len(coverage.Match))
	for i, match := range coverage.Match {
		ids[i] = match.ID
	}

	return
}

func main() {
	license := getElementByID("license")
	result := getElementByID("result")
	button := getElementByID("check")

	// Set the onclick handler for the visible button.
	button.Set("onclick", js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		ids := checkLicense(license.Get("value").String())
		result.Set("innerText", strings.Join(ids, ", "))
		return nil
	}))

	// The first check is extreme slow for some reason.
	// The loading screen is shown by default, so execute the check once,
	// and show the actual main body later.
	checkLicense("")
	getElementByID("loading").Get("style").Set("display", "none")
	getElementByID("checker").Get("style").Set("display", "block")

	select {}
}
