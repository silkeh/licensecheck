GO        = go
GOROOT    = $(shell go env GOROOT)

WASM_BUILD = env GOOS=js GOARCH=wasm $(GO) build
WASM_EXEC  = $(GOROOT)/misc/wasm/go_js_wasm_exec

all: public

public: public/wasm_exec.js public/licensecheck.wasm

wasm-test:
	$(GO) test -cover -v -exec="$(WASM_EXEC)" ./...

public/wasm_exec.js: $(GOROOT)/misc/wasm/wasm_exec.js
	cp $< $@

clean:
	rm -f public/*.wasm

%.wasm: %.go
	$(WASM_BUILD) -o $@ $<
